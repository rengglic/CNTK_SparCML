#ifndef _BUILDINFO_H
#define _BUILDINFO_H
#define _GIT_EXIST
#define _MATHLIB_ "mkl"
#define _BUILDSHA1_ "HEAD"
#define _BUILDBRANCH_ "HEAD"
#define _BUILDTARGET_ "GPU"
#define _CUDA_PATH_ "/usr/local/cuda-9.0"
#define _CUB_PATH_ "/home/rengglic/libs/cub-1.7.5"
#define _CUDNN_PATH_ "/home/rengglic/libs/cudnn-7.1"
#define _BUILDTYPE_ "release"
#define _WITH_ASGD_ "yes"
#define _BUILDER_ "rengglic"
#define _BUILDMACHINE_ "RengglicNb"
#define _BUILDPATH_ "/home/rengglic/repos/CNTK_SparCML"
#define _MPI_NAME_ "Unknown"
#define _MPI_VERSION_ "Unknown"
#endif
